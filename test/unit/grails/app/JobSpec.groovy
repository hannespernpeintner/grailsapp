package grails.app

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Job)
class JobSpec extends Specification {

	static Job job
    def setup() {
		
		job = new Job()
		job.title = "test"
		job.save()
    }

    def cleanup() {
    }

    void "test something"() {
		assertTrue(Job.all.contains(job))
    }
}
