<%@ page import="grails.app.Job" %>



<div class="fieldcontain ${hasErrors(bean: jobInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="job.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${jobInstance?.title}"/>

</div>

